module.exports = config:
	files:
		javascripts: joinTo:
			'js/vendor.js': /^bower_components/
			'js/app.js': /^app/
		stylesheets: joinTo: 'css/main.css'
		templates: joinTo: 'js/app.js'
	server:
		run: yes
	overrides:
		production:
			sourceMaps: true
	modules:
		nameCleaner: (path) ->
			# clear out app and js from the module names
			path = path.replace /^app\//, ''
			path.replace /^js\//, ''