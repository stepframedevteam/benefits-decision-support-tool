(function() {
  'use strict';

  var globals = typeof window === 'undefined' ? global : window;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var has = ({}).hasOwnProperty;

  var aliases = {};

  var endsWith = function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  };

  var unalias = function(alias, loaderPath) {
    var start = 0;
    if (loaderPath) {
      if (loaderPath.indexOf('components/' === 0)) {
        start = 'components/'.length;
      }
      if (loaderPath.indexOf('/', start) > 0) {
        loaderPath = loaderPath.substring(start, loaderPath.indexOf('/', start));
      }
    }
    var result = aliases[alias + '/index.js'] || aliases[loaderPath + '/deps/' + alias + '/index.js'];
    if (result) {
      return 'components/' + result.substring(0, result.length - '.js'.length);
    }
    return alias;
  };

  var expand = (function() {
    var reg = /^\.\.?(\/|$)/;
    return function(root, name) {
      var results = [], parts, part;
      parts = (reg.test(name) ? root + '/' + name : name).split('/');
      for (var i = 0, length = parts.length; i < length; i++) {
        part = parts[i];
        if (part === '..') {
          results.pop();
        } else if (part !== '.' && part !== '') {
          results.push(part);
        }
      }
      return results.join('/');
    };
  })();
  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';
    path = unalias(name, loaderPath);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has.call(cache, dirIndex)) return cache[dirIndex].exports;
    if (has.call(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  require.register = require.define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  require.list = function() {
    var result = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  require.brunch = true;
  globals.require = require;
})();
require.register("app", function(exports, require, module) {
/**
 * Main app module
 *
 * @module app
 * @description Main starting point for app. Sets up routing and many other features
 * that are required for the app to initialize.
 */

'use strict';

var dataHook = require('libs/data-hook');
var dataService = require('modules/data-service');
var slideLoader = require('modules/slide-loader');
var menu = require('modules/menu');

var app = {
	setup: setup
};

////////////

/**
 * Runs setup functions for various features and initializes Sammy.
 */
function setup() {
	dataHook.setup();
	menu.setup();
	dataService.setup();

	$.sammy('body', function() {

		$('.loader-image').hide();

		// s is for slide
		this.get('s/:sectionId/:pageId', slideLoader.routeHandler);
		this.get('', slideLoader.routeHandler);
	}).run();
}

module.exports = app;

});

require.register("libs/data-hook", function(exports, require, module) {
/**
 * jQuery Data Hook module
 *
 * @module lib/data-hook
 * @description Sets up the jQuery data hook plugin that is outlined {@link http://www.sitepoint.com/effective-event-binding-jquery/}
 * Example usage:<br>
 * <pre><code>
 * // Create button with a data-hook attribute
 * &lt;button data-hook=&quot;nav-menu-toggle&quot;&gt;Toggle Nav&lt;/button&gt;
 *
 * // Select that button as follows:
 * $.hook('nav-menu-toggle');
 *
 * // Register an event handler as follows:
 * $.hook('nav-menu-toggle').on('click', function() {});
 * </code></pre>

 */

'use strict';

var dataHook = {
	setup: setup
};

/**
 * Registers the $.hook jQuery plugin
 */
function setup() {

	$.extend({
		hook: function(hookName) {
			var selector;
			if (!hookName || hookName === '*') {
				// select all data-hooks
				selector = '[data-hook]';
			} else {
				// select specific data-hook
				selector = '[data-hook~="' + hookName + '"]';
			}
			return $(selector);
		}
	});
}

module.exports = dataHook;

});

require.register("modules/animate", function(exports, require, module) {
/**
 * Animation module
 *
 * @module modules/animate
 * @description Handles greensock animations of slide elements.
 */

'use strict';

var animate = {
	out: out
};

/**
 * Animates slide elements out of view in the manner specified by their exit_[direction] class
 * @param  {jQuery.object} $container jQuery object of the container for the current slide.
 * @return {jQuery.promise}
 */
function out($container) {

	var defer = new $.Deferred();
	var timer = 0.3;
	var distance = 20;
	var easing = Power2.easeInOut;
	var completeCount = 0;
	var animCount = 5;
	var $left = $container.find('.exit_left');
	var $right = $container.find('.exit_right');
	var $up = $container.find('.exit_up');
	var $down = $container.find('.exit_down');

	// Animate each element
	TweenMax.to($container, timer, {
		opacity: 0,
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($left, timer, {
		opacity: 0,
		left: '-=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($right, timer, {
		opacity: 0,
		left: '+=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($up, timer, {
		opacity: 0,
		top: '-=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($down, timer, {
		opacity: 0,
		top: '+=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});

	// promise will be resolved when all animations finish
	return defer.promise();

	// Increment and check if all animations are complete
	function completeCheck() {
		completeCount++;

		if (completeCount === animCount) {
			animCleanup();
		}
	}

	// Update elements back to their state prior to the animations
	function animCleanup() {

		console.log('left before:', $left.css('left'));
		console.log('up before:', $up.css('top'));
		console.log('down before:', $down.css('top'));

		$left.length && $left.css('left', (parseInt($left.css('left'), 10) + distance) + 'px');
		$right.length && $right.css('left', (parseInt($right.css('left'), 10) - distance) + 'px');
		$up.length && $up.css('top', (parseInt($up.css('top'), 10) + distance) + 'px');
		$down.length && $down.css('top', (parseInt($down.css('top'), 10) - distance) + 'px');
		$container.length && $container.css('opacity', 1);

		console.log('left after:', $left.css('left'));
		console.log('up after:', $up.css('top'));
		console.log('down after:', $down.css('top'));

		defer.resolve();
	}
}

module.exports = animate;

});

require.register("modules/buttons", function(exports, require, module) {
/**
 * Button module
 *
 * @module modules/buttons
 * @description Loads and caches button data. Injects buttons into slides.
 */

'use strict';

var buttonTmpl = require('views/slide-buttons');
var comp = require('modules/composition');
var dataService = require('modules/data-service');

var $btnContainer = false;

var appData = {};
var buttonData = {};
var Edge = AdobeEdge;
var isDataLoaded = false;
var stage;

var buttons = {
	setup: setup,
	updateContainer: updateContainer,
	hideButtons: hideButtons
};

////////////

/**
 * Loads button data if needed, and sets up all button events
 *
 * @param {String} sectionId The sectionId of the section in which the current slide lives. (e.g. Medical, Dental, etc.)
 * @param {String} pageId The pageId of the current slide.
 */
function setup(sectionId, pageId, isFirstSlide) {

	appData = dataService.getAppData();
	stage = comp.current.getStage();

	$btnContainer = $('.' + comp.current.id + ' .button-list');

	if ($btnContainer.length > 0) {
		stage.stop();
		$.when(loadDataForSection().done($.proxy(dataLoaded, buttons.setup)));
	}

	function loadDataForSection() {

		return $.ajax({
			dataType: 'json',
			url: 'data/' + sectionId + '-buttons.json',
			success: function(data) {

				if (!buttonData[sectionId]) {
					buttonData[sectionId] = {};
				}

				buttonData[sectionId] = data.slides;
				cleanData(sectionId);
				isDataLoaded = true;
			},
			error: function() {
				console.log('problem loading button data');
			}
		});
	}

	function dataLoaded() {

		var buttonTime = 0;

		if ($btnContainer.length && !$btnContainer.data('has-buttons')) {
			try {
				addToSlide(sectionId, pageId);

				// Setup an event for animating in the buttons
				buttonTime = stage.getLabelPosition('Buttons');
				Edge.Symbol.bindTriggerAction(comp.current.id, 'stage', 'Default Timeline', buttonTime, buttonTriggerHandler);

				if (isFirstSlide) {
					stage.play();
				}
			} catch(e) {
				// not empty
				console.log('error: ',  e);
			}
		}
	}

	function buttonTriggerHandler() {

		var $buttons = $btnContainer.find('.button');
		console.log('show buttons', comp.current.id);

		$btnContainer.addClass('is-loaded');
		if ($btnContainer.hasClass('stagger')) {
			TweenMax.staggerFromTo($buttons, 1, {opacity: 0}, {opacity: 1}, 0.25);
		}
	}
}

/**
 * Cleans the button data to remove buttons to which the current user does not have access.
 */
function cleanData(sectionId) {

	var i;
	var pageId;
	var curItem;

	for (pageId in buttonData[sectionId]) {

		curItem = buttonData[sectionId][pageId];

		for (i = 0; i < curItem.length; i++) {
			if (appData.userType.trim()) {
				if (curItem[i].restrict && curItem[i].restrict.indexOf(appData.userType) !== -1) {
					curItem[i] = {};
				}
			}
		}
	}
}

/**
 * Adds buttons to the current slide.
 * @param {String} sectionId The sectionId of the section in which the current slide lives. (e.g. Medical, Dental, etc.)
 * @param {String} pageId The pageId of the current slide.
 */
function addToSlide(sectionId, pageId) {

	var tmpl;

	// Add the buttons
	if (buttonData[sectionId] && buttonData[sectionId][pageId]) {

		tmpl = buttonTmpl({buttons: buttonData[sectionId][pageId]});

		$btnContainer
			.append(tmpl)
			.data('has-buttons', true);
	}
}

/**
 * Update the $btnContainer object to be pointing at the current composition object's containing element.
 */
function updateContainer() {

	$btnContainer = $('.' + comp.current.id + ' .button-list');
	hideButtons();
}

function hideButtons() {

	if ($btnContainer.hasClass('stagger')) {
		$btnContainer.find('.button').css({opacity: 0});
	}
}

module.exports = buttons;

});

require.register("modules/composition", function(exports, require, module) {
/**
 * Composition module
 *
 * @module modules/composition
 * @description Houses current composition object, so it's easily shareable between other modules
 */

'use strict';

/**
 * Composition object. <br>{@link https://www.adobe.com/devnet-docs/edgeanimate/api/current/index.html#compositioninstance}
 * @property {AdobeEdge.Composition} composition.current - The currently displayed AdobeEdge composition
 */
var composition = {
	current: null
};

module.exports = composition;

});

require.register("modules/data-service", function(exports, require, module) {
/**
 * Data module
 *
 * @module modules/data-service
 * @description Captures app data and injects data into slides
 */

'use strict';

var comp = require('modules/composition');

var appData = {};
var isDataLoaded = false;

var dataService = {
	setup: setup,
	inject: inject,
	getAppData: getAppData
};

////////////

/**
 * Loads and caches app data
 */
function setup() {

	$.when(loadData().done());

	function loadData() {

		return $.ajax({
			dataType: 'json',
			url: 'data/data.json',
			success: function(data) {

				appData = data;
			},
			error: function() {
				console.log('problem loading site data');
			}
		});
	}
}

/**
 * Injects app data into slides
 */
function inject() {

	var key;

	// loop through anything with the inject class on it and replace text using the {{key-name}} syntax
	$('.inject').each(function() {

		for (key in appData) {

			this.innerHTML = this.innerHTML.replace('{{' + key + '}}', appData[key]);
		}
	});
}

function getAppData() {

	return appData;
}

module.exports = dataService;

});

require.register("modules/menu", function(exports, require, module) {
/**
 * Menu module
 *
 * @module modules/menu
 * @description Initializes menu and handles all menu events.
 */

'use strict';

var menuView = require('views/menu');

var $menuWrapper = $('.side-menu');

var menu = {
	setup: setup,
	toggle: toggleClickHandler,
	isOpened: isOpened
};

////////////

/**
 * Initializes and sets up event handlers for the menu.
 */
function setup() {

	$menuWrapper.html(menuView);
	$.hook('menu-toggle').on('click', toggleClickHandler);
	$.hook('menu-item').on('click', subMenuItemClickHandler);
}

/**
 * Event handler for toggling the menu open/closed.
 * @param  {jQuery.Event} e jQuery event object.
 */
function toggleClickHandler(e) {

	e && e.preventDefault();
	$menuWrapper.toggleClass('is-opened');
}

/**
 * Returns true if the menu is currently open. Otherwise, returns false.
 * @return {Boolean} Boolean indicating whether the menu is opened.
 */
function isOpened() {

	return $menuWrapper.hasClass('is-opened');
}

/**
 * Event handler for toggling sub-menu items open/closed.
 * @param  {jQuery.Event} e jQuery event object
 */
function subMenuItemClickHandler(e) {

	var $curSubMenu = $(this).siblings('.sub-menu');

	e && e.preventDefault();

	// Close all other sub-menus
	$('.sub-menu').not($curSubMenu).slideUp();

	// Toggle current sub-menu
	$curSubMenu.slideToggle();
}

module.exports = menu;

});

require.register("modules/progress-bar", function(exports, require, module) {
/**
 * Progress bar module
 *
 * @module modules/progress-bar
 * @description Handles all events and logic related to progress bar, including click, drag,
 * and timeline events.
 */

'use strict';

var comp = require('modules/composition');
var slideEvents = require('modules/slide-events');
var video = require('modules/video');

var $positionIndicator = $('.progress-bar_position');

var duration;
var Edge = AdobeEdge;
var slideWidth = 981;
var stage;
var wasPlaying = false;
var xLocation;

var progressBar = {
	setup: setup,
	show: show
};

////////////

/**
 * Sets up event handlers for the progress bar.
 */
function setup() {

	// Events
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'update', updateHandler);
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'play', playHandler);

	$('.progress-bar').on('click', progressBarClickHandler);

	$positionIndicator.draggable({
		axis: 'x',
		containment: 'parent',
		start: function() {

			wasPlaying = stage.isPlaying();
		},
		drag: function(e, ui) {

			stage = comp.current.getStage();
			updateSlideLocation(ui.position.left);
		},
		stop: function() {

			stage = comp.current.getStage();

			if (wasPlaying) {
				stage.play(location);
			}
		}
	});
}

function show() {

	$('.progress-bar_container').fadeIn();
}

/**
 * Stops the current composition at the specified leftPosition.
 * @param  {String} leftPosition The left location of the progress indicator, which is used to calculate
 * the location where the slide should be stopped.
 */
function updateSlideLocation(leftPosition) {

	xLocation = parseInt((leftPosition / slideWidth) * duration, 10);
	stage.stop(xLocation);
}

// Event Handlers

/**
 * Handles composition timeline updates, updating the position of the progress bar indicator.
 * @param  {AdobeEdge.Symbol} sym The Symbol instance of the current AdobeEdge composition.
 * @param  {jQuery.Event} e   jQuery event object.
 */
function updateHandler(sym, e) {

	var elapsed = e.elapsed;
	var percentDone;

	if (elapsed === 0) {
		percentDone = 0;
	} else {
		percentDone = (elapsed / duration) * 100;
	}

	$('.progress-bar_position').css('left', percentDone + '%');
	$('.progress-bar_progress').css('width', percentDone + '%');
}

/**
 * Handles the composition timeline play event. Updates stage and duration vars to match current composition.
 */
function playHandler() {

	stage = comp.current.getStage();
	duration = stage.getDuration();
}

/**
 * Handles progress bar click events, updating the progress bar indicator and slide location.
 * @param  {jQuery.Event} e   jQuery event object.
 */
function progressBarClickHandler(e) {

	wasPlaying = stage.isPlaying();

	$positionIndicator.offset({left: e.offsetX});
	updateSlideLocation(e.offsetX);

	if (wasPlaying) {
		slideEvents.playPauseButton();
		// stage.play();
	}
}


module.exports = progressBar;

});

require.register("modules/slide-events", function(exports, require, module) {
/**
 * Slide event module
 *
 * @module modules/slide-events
 * @description Handles all events related to the current slide
 */

'use strict';

var comp = require('modules/composition');
var buttons = require('modules/buttons');

var slideEvents = {
	setup: setup,
	timelineSetup: timelineSetup,
	playPauseButton: playPauseButtonHandler
};

var Edge = AdobeEdge;

////////////

/**
 * Sets up slide related events.
 */
function setup() {

	// Events
	$.hook('button_play-pause').on('click', playPauseButtonHandler);
}

function timelineSetup() {

	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'complete', completeHandler);
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'play', playHandler);
}

// Event Handlers

/**
 * Toggles between play/pause for the current composition.
 */
function playPauseButtonHandler(e) {

	var stage = comp.current.getStage();

	e && e.preventDefault();

	if (stage.isPlaying()) {
		$.hook('button_play-pause').toggleClass('is-playing');
		stage.stop();
	} else {
		$.hook('button_play-pause').toggleClass('is-playing');
		stage.play();
	}
}

function playHandler() {

	$.hook('button_play-pause').addClass('is-playing');
	buttons.hideButtons();
}

function completeHandler() {

	$.hook('button_play-pause').removeClass('is-playing');
}

module.exports = slideEvents;

});

require.register("modules/slide-loader", function(exports, require, module) {
/**
 * Slide loader module
 *
 * @module modules/slide-loader
 * @description Provides a routeHandler that loads slides and calls other modules to
 * setup slide logic
 */

'use strict';

var animate = require('modules/animate');
var buttons = require('modules/buttons');
var comp = require('modules/composition');
var dataService = require('modules/data-service');
var menu = require('modules/menu');
var progressBar = require('modules/progress-bar');
var slideEvents = require('modules/slide-events');
var slideWrapperTmpl = require('views/slide-wrapper');
var video = require('modules/video');

var $container;
var $prevContainer;

var Edge = AdobeEdge;
var eventsInitialized = false;
var isFirstSlide = true;
var pageId = 'a1';
var path;
var prevStage;
var sectionId = 'intro';
var slideClass;
var stage;

////////////

var slideLoader = {
	routeHandler: routeHandler
};

/**
 * Handles a sammy route that needs to load an Edge Animate slide
 * @param  {Sammy.EventContext} context The sammyjs event context for the current route.
 * {@link http://sammyjs.org/docs/api/0.7.4/all#Sammy.EventContext}
 */
function routeHandler(context) {

	$('.loader-image').show();

	if (menu.isOpened()) {
		menu.toggle();
	}

	prevStage = stage;
	$prevContainer = $container;

	// capture context details, using intro/a1 as the default
	pageId = context.params.pageId || 'a1';
	sectionId = context.params.sectionId || 'intro';

	slideClass = sectionId + '_' + pageId;
	path = 'slides/' + sectionId + '/' + slideClass;
	$container = $('.' + slideClass);

	if ($container && $container.length) {

		// Slide exists, so find and play it
		comp.current = Edge.getComposition(slideClass);
		stage = comp.current.getStage();
		buttons.updateContainer();
		$('.loader-image').hide();
	} else {

		// Slide doesn't exist, so load it
		loadSlide();
		$container = $('.' + slideClass);
	}

	// If needed, cleanup previous slide
	if (comp.current != null) {

		// out function returns a promise, so show the slide after that
		animate.out($prevContainer).then(function() {
			$container.css('opacity', 0);
			showSlide();
			stage.play(0);

			TweenMax.to($container, 0.3, {opacity: 1, easing: Power2.easeInOut});
		});
		cleanUpPreviousSlide();
	} else {
		showSlide();
	}

	// Initialize events only once
	if (!eventsInitialized) {

		eventsInitialized = true;
		slideEvents.setup();
		progressBar.show();
		Edge.bootstrapCallback(bootstrapCallbackHandler);
	}
}

function showSlide() {

	// Show only the current slide
	$('.slide')
		.not($container).hide()
		.parents('.flow-wrapper').hide();
	$container.show()
		.parents('.flow-wrapper').show();
}

/**
 * Hides dynamically added elements and stops the previous slide
 */
function cleanUpPreviousSlide() {

	video.pause();
	prevStage.stop();
}

/**
 * Creates a container element for a new slide and then loads the new slide.
 */
function loadSlide() {

	// Add wrapper element for current slide
	$('.slide-container').prepend(slideWrapperTmpl({class: slideClass}));

	// Load the slide
	Edge.loadComposition(path, slideClass, {
		scaleToFit: 'none',
		centerStage: 'none',
		minW: '0',
		maxW: 'undefined',
		width: '981px',
		height: '475px'
	}, {dom: [ ]}, {dom: [ ]});
}

/**
 * Sets up the compositionReady event, which runs initialization code whenever a new slide is loaded.
 * @param  {String} compId The ID of the newly loaded AdobeEdge.Composition object.
 */
function bootstrapCallbackHandler(compId) {

	Edge.Symbol.bindElementAction(compId, 'stage', 'document', 'compositionReady', function() {

		$('.loader-image').hide();

		comp.current = Edge.getComposition(compId);
		stage = comp.current.getStage();

		buttons.setup(sectionId, pageId, isFirstSlide);
		dataService.inject();

		// PROGRESS BAR -- FOR TESTING
		progressBar.setup();
		slideEvents.timelineSetup();

		isFirstSlide = false;
	});
}

module.exports = slideLoader;

});

require.register("modules/video", function(exports, require, module) {
/**
 * Video module
 *
 * @module modules/video
 * @description Handles all video related logic.
 */

'use strict';

var comp = require('modules/composition');

var video = {
	pause: pause,
	play: play,
	getCurrent: getCurrent
};


/**
 * Pauses the current video.
 */
function pause() {

	var curVideo = getCurrent();

	if (curVideo) {
		try {
			curVideo.pause();
		} catch (e) {
			// video probably doesn't exist
		}
	}
}

/**
 * Plays the video on the current slide.
 */
function play() {

	var curVideo = getCurrent();

	if (curVideo) {
		try {
			curVideo.play();
		} catch (e) {
			// video probably doesn't exist
		}
	}
}

/**
 * Looks up the video for the current slide and returns the video.
 * @return {video} The video from the current slide, or null if none exists.
 */
function getCurrent() {

	var $video = $('.' + comp.current.id + ' .myVideo');

	if ($video.length) {
		return $video[0];
	}

	return null;
}

module.exports = video;

});

require.register("views/menu", function(exports, require, module) {
var __templateData = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<a class=\"menu_button\" data-hook=\"menu-toggle\" href=\"\"><span class=\"icon icon-menu\"></span></a>\r\n\r\n<div class=\"menu_wrapper\">\r\n	<a class=\"menu_button menu_email-button\" href=\"\"><span class=\"icon icon-envelope\"></span></a>\r\n	<ul class=\"menu\">\r\n 		<li class=\"menu_item\">\r\n			<a href=\"./#/\" class=\"menu_link\"><span class=\"icon icon-asterisk\"></span> Intro and Benefits Profile</a>\r\n		</li>\r\n		<li class=\"menu_item\">\r\n			<a href=\"./#/\" class=\"menu_link\"><span class=\"icon icon-leaf\"></span> Enrollment</a>\r\n		</li>\r\n		<li class=\"menu_item\">\r\n			<a href=\"./#/\" data-hook=\"menu-item\" class=\"menu_link\"><span class=\"icon icon-user-md\"></span> Health &amp; Wellness</a>\r\n			<ul class=\"sub-menu\">\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/medical/intro\">Medical</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/dental/intro\">Dental</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/vision/intro\">Vision</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/wellness/intro\">HCA Wellness Program</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/eap/intro\">Employee Assistance Program</a>\r\n				</li>\r\n			</ul>\r\n		</li>\r\n		<li class=\"menu_item\">\r\n			<a href=\"./#/\" data-hook=\"menu-item\" class=\"menu_link\"><span class=\"icon icon-group\"></span> Security &amp; Protection</a>\r\n			<ul class=\"sub-menu\">\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/life-and-ad&d/intro\">Life and AD&amp;D</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/std/intro\">Short-Term Disability (TAFW)</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/ltd/intro\">Long-Term Disability</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/coreplus/intro\">CorePlus Benefits</a>\r\n				</li>\r\n			</ul>\r\n		</li>\r\n		<li class=\"menu_item\">\r\n			<a href=\"./#/\" data-hook=\"menu-item\" class=\"menu_link\"><span class=\"icon icon-coin-dollar\"></span> Retirement &amp; Money</a>\r\n			<ul class=\"sub-menu\">\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/401k/intro\">HCA 401(k) Plan</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/fsa/intro\">Flexible Spending Accounts (FSAs)</a>\r\n				</li>\r\n				<li class=\"sub-menu_item\">\r\n					<a class=\"menu_link\" href=\"./#/s/financial-fitness-program/intro\">HCA Financial Fitness Program</a>\r\n				</li>\r\n			</ul>\r\n		</li>\r\n		<li class=\"menu_item\">\r\n			<a href=\"./#/\" class=\"menu_link\"><span class=\"icon icon-user\"></span> HCA Resources</a>\r\n		</li>\r\n	</ul>\r\n</div>";
  },"useData":true});
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/slide-buttons", function(exports, require, module) {
var __templateData = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.text : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<a class=\"button "
    + escapeExpression(((helper = (helper = helpers['class'] || (depth0 != null ? depth0['class'] : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"class","hash":{},"data":data}) : helper)))
    + "\" href=\""
    + escapeExpression(((helper = (helper = helpers.href || (depth0 != null ? depth0.href : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"href","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"text","hash":{},"data":data}) : helper)))
    + "</a><br>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.buttons : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/slide-wrapper", function(exports, require, module) {
var __templateData = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"slide "
    + escapeExpression(((helper = (helper = helpers['class'] || (depth0 != null ? depth0['class'] : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"class","hash":{},"data":data}) : helper)))
    + "\"></div>";
},"useData":true});
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;
//# sourceMappingURL=app.js.map