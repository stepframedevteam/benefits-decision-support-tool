/**
 * Main app module
 *
 * @module app
 * @description Main starting point for app. Sets up routing and many other features
 * that are required for the app to initialize.
 */

'use strict';

var dataHook = require('libs/data-hook');
var dataService = require('modules/data-service');
var slideLoader = require('modules/slide-loader');
var menu = require('modules/menu');

var app = {
	setup: setup
};

////////////

/**
 * Runs setup functions for various features and initializes Sammy.
 */
function setup() {
	dataHook.setup();
	menu.setup();
	dataService.setup();

	$.sammy('body', function() {

		$('.loader-image').hide();

		// s is for slide
		this.get('s/:sectionId/:pageId', slideLoader.routeHandler);
		this.get('', slideLoader.routeHandler);
	}).run();
}

module.exports = app;
