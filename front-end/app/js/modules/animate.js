/**
 * Animation module
 *
 * @module modules/animate
 * @description Handles greensock animations of slide elements.
 */

'use strict';

var animate = {
	out: out
};

/**
 * Animates slide elements out of view in the manner specified by their exit_[direction] class
 * @param  {jQuery.object} $container jQuery object of the container for the current slide.
 * @return {jQuery.promise}
 */
function out($container) {

	var defer = new $.Deferred();
	var timer = 0.3;
	var distance = 20;
	var easing = Power2.easeInOut;
	var completeCount = 0;
	var animCount = 5;
	var $left = $container.find('.exit_left');
	var $right = $container.find('.exit_right');
	var $up = $container.find('.exit_up');
	var $down = $container.find('.exit_down');

	// Animate each element
	TweenMax.to($container, timer, {
		opacity: 0,
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($left, timer, {
		opacity: 0,
		left: '-=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($right, timer, {
		opacity: 0,
		left: '+=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($up, timer, {
		opacity: 0,
		top: '-=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});
	TweenMax.to($down, timer, {
		opacity: 0,
		top: '+=' + distance + 'px',
		ease: easing,
		onComplete: completeCheck
	});

	// promise will be resolved when all animations finish
	return defer.promise();

	// Increment and check if all animations are complete
	function completeCheck() {
		completeCount++;

		if (completeCount === animCount) {
			animCleanup();
		}
	}

	// Update elements back to their state prior to the animations
	function animCleanup() {

		console.log('left before:', $left.css('left'));
		console.log('up before:', $up.css('top'));
		console.log('down before:', $down.css('top'));

		$left.length && $left.css('left', (parseInt($left.css('left'), 10) + distance) + 'px');
		$right.length && $right.css('left', (parseInt($right.css('left'), 10) - distance) + 'px');
		$up.length && $up.css('top', (parseInt($up.css('top'), 10) + distance) + 'px');
		$down.length && $down.css('top', (parseInt($down.css('top'), 10) - distance) + 'px');
		$container.length && $container.css('opacity', 1);

		console.log('left after:', $left.css('left'));
		console.log('up after:', $up.css('top'));
		console.log('down after:', $down.css('top'));

		defer.resolve();
	}
}

module.exports = animate;
