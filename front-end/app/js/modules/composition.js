/**
 * Composition module
 *
 * @module modules/composition
 * @description Houses current composition object, so it's easily shareable between other modules
 */

'use strict';

/**
 * Composition object. <br>{@link https://www.adobe.com/devnet-docs/edgeanimate/api/current/index.html#compositioninstance}
 * @property {AdobeEdge.Composition} composition.current - The currently displayed AdobeEdge composition
 */
var composition = {
	current: null
};

module.exports = composition;
