/**
 * Data module
 *
 * @module modules/data-service
 * @description Captures app data and injects data into slides
 */

'use strict';

var comp = require('modules/composition');

var appData = {};
var isDataLoaded = false;

var dataService = {
	setup: setup,
	inject: inject,
	getAppData: getAppData
};

////////////

/**
 * Loads and caches app data
 */
function setup() {

	$.when(loadData().done());

	function loadData() {

		return $.ajax({
			dataType: 'json',
			url: 'data/data.json',
			success: function(data) {

				appData = data;
			},
			error: function() {
				console.log('problem loading site data');
			}
		});
	}
}

/**
 * Injects app data into slides
 */
function inject() {

	var key;

	// loop through anything with the inject class on it and replace text using the {{key-name}} syntax
	$('.inject').each(function() {

		for (key in appData) {

			this.innerHTML = this.innerHTML.replace('{{' + key + '}}', appData[key]);
		}
	});
}

function getAppData() {

	return appData;
}

module.exports = dataService;
