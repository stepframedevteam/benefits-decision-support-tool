/**
 * Video module
 *
 * @module modules/video
 * @description Handles all video related logic.
 */

'use strict';

var comp = require('modules/composition');

var video = {
	pause: pause,
	play: play,
	getCurrent: getCurrent
};


/**
 * Pauses the current video.
 */
function pause() {

	var curVideo = getCurrent();

	if (curVideo) {
		try {
			curVideo.pause();
		} catch (e) {
			// video probably doesn't exist
		}
	}
}

/**
 * Plays the video on the current slide.
 */
function play() {

	var curVideo = getCurrent();

	if (curVideo) {
		try {
			curVideo.play();
		} catch (e) {
			// video probably doesn't exist
		}
	}
}

/**
 * Looks up the video for the current slide and returns the video.
 * @return {video} The video from the current slide, or null if none exists.
 */
function getCurrent() {

	var $video = $('.' + comp.current.id + ' .myVideo');

	if ($video.length) {
		return $video[0];
	}

	return null;
}

module.exports = video;
