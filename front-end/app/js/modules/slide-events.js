/**
 * Slide event module
 *
 * @module modules/slide-events
 * @description Handles all events related to the current slide
 */

'use strict';

var comp = require('modules/composition');
var buttons = require('modules/buttons');

var slideEvents = {
	setup: setup,
	timelineSetup: timelineSetup,
	playPauseButton: playPauseButtonHandler
};

var Edge = AdobeEdge;

////////////

/**
 * Sets up slide related events.
 */
function setup() {

	// Events
	$.hook('button_play-pause').on('click', playPauseButtonHandler);
}

function timelineSetup() {

	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'complete', completeHandler);
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'play', playHandler);
}

// Event Handlers

/**
 * Toggles between play/pause for the current composition.
 */
function playPauseButtonHandler(e) {

	var stage = comp.current.getStage();

	e && e.preventDefault();

	if (stage.isPlaying()) {
		$.hook('button_play-pause').toggleClass('is-playing');
		stage.stop();
	} else {
		$.hook('button_play-pause').toggleClass('is-playing');
		stage.play();
	}
}

function playHandler() {

	$.hook('button_play-pause').addClass('is-playing');
	buttons.hideButtons();
}

function completeHandler() {

	$.hook('button_play-pause').removeClass('is-playing');
}

module.exports = slideEvents;
