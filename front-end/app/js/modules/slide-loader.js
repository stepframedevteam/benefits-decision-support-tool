/**
 * Slide loader module
 *
 * @module modules/slide-loader
 * @description Provides a routeHandler that loads slides and calls other modules to
 * setup slide logic
 */

'use strict';

var animate = require('modules/animate');
var buttons = require('modules/buttons');
var comp = require('modules/composition');
var dataService = require('modules/data-service');
var menu = require('modules/menu');
var progressBar = require('modules/progress-bar');
var slideEvents = require('modules/slide-events');
var slideWrapperTmpl = require('views/slide-wrapper');
var video = require('modules/video');

var $container;
var $prevContainer;

var Edge = AdobeEdge;
var eventsInitialized = false;
var isFirstSlide = true;
var pageId = 'a1';
var path;
var prevStage;
var sectionId = 'intro';
var slideClass;
var stage;

////////////

var slideLoader = {
	routeHandler: routeHandler
};

/**
 * Handles a sammy route that needs to load an Edge Animate slide
 * @param  {Sammy.EventContext} context The sammyjs event context for the current route.
 * {@link http://sammyjs.org/docs/api/0.7.4/all#Sammy.EventContext}
 */
function routeHandler(context) {

	$('.loader-image').show();

	if (menu.isOpened()) {
		menu.toggle();
	}

	prevStage = stage;
	$prevContainer = $container;

	// capture context details, using intro/a1 as the default
	pageId = context.params.pageId || 'a1';
	sectionId = context.params.sectionId || 'intro';

	slideClass = sectionId + '_' + pageId;
	path = 'slides/' + sectionId + '/' + slideClass;
	$container = $('.' + slideClass);

	if ($container && $container.length) {

		// Slide exists, so find and play it
		comp.current = Edge.getComposition(slideClass);
		stage = comp.current.getStage();
		buttons.updateContainer();
		$('.loader-image').hide();
	} else {

		// Slide doesn't exist, so load it
		loadSlide();
		$container = $('.' + slideClass);
	}

	// If needed, cleanup previous slide
	if (comp.current != null) {

		// out function returns a promise, so show the slide after that
		animate.out($prevContainer).then(function() {
			$container.css('opacity', 0);
			showSlide();
			stage.play(0);

			TweenMax.to($container, 0.3, {opacity: 1, easing: Power2.easeInOut});
		});
		cleanUpPreviousSlide();
	} else {
		showSlide();
	}

	// Initialize events only once
	if (!eventsInitialized) {

		eventsInitialized = true;
		slideEvents.setup();
		progressBar.show();
		Edge.bootstrapCallback(bootstrapCallbackHandler);
	}
}

function showSlide() {

	// Show only the current slide
	$('.slide')
		.not($container).hide()
		.parents('.flow-wrapper').hide();
	$container.show()
		.parents('.flow-wrapper').show();
}

/**
 * Hides dynamically added elements and stops the previous slide
 */
function cleanUpPreviousSlide() {

	video.pause();
	prevStage.stop();
}

/**
 * Creates a container element for a new slide and then loads the new slide.
 */
function loadSlide() {

	// Add wrapper element for current slide
	$('.slide-container').prepend(slideWrapperTmpl({class: slideClass}));

	// Load the slide
	Edge.loadComposition(path, slideClass, {
		scaleToFit: 'none',
		centerStage: 'none',
		minW: '0',
		maxW: 'undefined',
		width: '981px',
		height: '475px'
	}, {dom: [ ]}, {dom: [ ]});
}

/**
 * Sets up the compositionReady event, which runs initialization code whenever a new slide is loaded.
 * @param  {String} compId The ID of the newly loaded AdobeEdge.Composition object.
 */
function bootstrapCallbackHandler(compId) {

	Edge.Symbol.bindElementAction(compId, 'stage', 'document', 'compositionReady', function() {

		$('.loader-image').hide();

		comp.current = Edge.getComposition(compId);
		stage = comp.current.getStage();

		buttons.setup(sectionId, pageId, isFirstSlide);
		dataService.inject();

		// PROGRESS BAR -- FOR TESTING
		progressBar.setup();
		slideEvents.timelineSetup();

		isFirstSlide = false;
	});
}

module.exports = slideLoader;
