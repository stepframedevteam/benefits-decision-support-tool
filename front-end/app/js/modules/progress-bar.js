/**
 * Progress bar module
 *
 * @module modules/progress-bar
 * @description Handles all events and logic related to progress bar, including click, drag,
 * and timeline events.
 */

'use strict';

var comp = require('modules/composition');
var slideEvents = require('modules/slide-events');
var video = require('modules/video');

var $positionIndicator = $('.progress-bar_position');

var duration;
var Edge = AdobeEdge;
var slideWidth = 981;
var stage;
var wasPlaying = false;
var xLocation;

var progressBar = {
	setup: setup,
	show: show
};

////////////

/**
 * Sets up event handlers for the progress bar.
 */
function setup() {

	// Events
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'update', updateHandler);
	Edge.Symbol.bindTimelineAction(comp.current.id, 'stage', 'Default Timeline', 'play', playHandler);

	$('.progress-bar').on('click', progressBarClickHandler);

	$positionIndicator.draggable({
		axis: 'x',
		containment: 'parent',
		start: function() {

			wasPlaying = stage.isPlaying();
		},
		drag: function(e, ui) {

			stage = comp.current.getStage();
			updateSlideLocation(ui.position.left);
		},
		stop: function() {

			stage = comp.current.getStage();

			if (wasPlaying) {
				stage.play(location);
			}
		}
	});
}

function show() {

	$('.progress-bar_container').fadeIn();
}

/**
 * Stops the current composition at the specified leftPosition.
 * @param  {String} leftPosition The left location of the progress indicator, which is used to calculate
 * the location where the slide should be stopped.
 */
function updateSlideLocation(leftPosition) {

	xLocation = parseInt((leftPosition / slideWidth) * duration, 10);
	stage.stop(xLocation);
}

// Event Handlers

/**
 * Handles composition timeline updates, updating the position of the progress bar indicator.
 * @param  {AdobeEdge.Symbol} sym The Symbol instance of the current AdobeEdge composition.
 * @param  {jQuery.Event} e   jQuery event object.
 */
function updateHandler(sym, e) {

	var elapsed = e.elapsed;
	var percentDone;

	if (elapsed === 0) {
		percentDone = 0;
	} else {
		percentDone = (elapsed / duration) * 100;
	}

	$('.progress-bar_position').css('left', percentDone + '%');
	$('.progress-bar_progress').css('width', percentDone + '%');
}

/**
 * Handles the composition timeline play event. Updates stage and duration vars to match current composition.
 */
function playHandler() {

	stage = comp.current.getStage();
	duration = stage.getDuration();
}

/**
 * Handles progress bar click events, updating the progress bar indicator and slide location.
 * @param  {jQuery.Event} e   jQuery event object.
 */
function progressBarClickHandler(e) {

	wasPlaying = stage.isPlaying();

	$positionIndicator.offset({left: e.offsetX});
	updateSlideLocation(e.offsetX);

	if (wasPlaying) {
		slideEvents.playPauseButton();
		// stage.play();
	}
}


module.exports = progressBar;
