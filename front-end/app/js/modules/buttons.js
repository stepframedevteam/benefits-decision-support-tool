/**
 * Button module
 *
 * @module modules/buttons
 * @description Loads and caches button data. Injects buttons into slides.
 */

'use strict';

var buttonTmpl = require('views/slide-buttons');
var comp = require('modules/composition');
var dataService = require('modules/data-service');

var $btnContainer = false;

var appData = {};
var buttonData = {};
var Edge = AdobeEdge;
var isDataLoaded = false;
var stage;

var buttons = {
	setup: setup,
	updateContainer: updateContainer,
	hideButtons: hideButtons
};

////////////

/**
 * Loads button data if needed, and sets up all button events
 *
 * @param {String} sectionId The sectionId of the section in which the current slide lives. (e.g. Medical, Dental, etc.)
 * @param {String} pageId The pageId of the current slide.
 */
function setup(sectionId, pageId, isFirstSlide) {

	appData = dataService.getAppData();
	stage = comp.current.getStage();

	$btnContainer = $('.' + comp.current.id + ' .button-list');

	if ($btnContainer.length > 0) {
		stage.stop();
		$.when(loadDataForSection().done($.proxy(dataLoaded, buttons.setup)));
	}

	function loadDataForSection() {

		return $.ajax({
			dataType: 'json',
			url: 'data/' + sectionId + '-buttons.json',
			success: function(data) {

				if (!buttonData[sectionId]) {
					buttonData[sectionId] = {};
				}

				buttonData[sectionId] = data.slides;
				cleanData(sectionId);
				isDataLoaded = true;
			},
			error: function() {
				console.log('problem loading button data');
			}
		});
	}

	function dataLoaded() {

		var buttonTime = 0;

		if ($btnContainer.length && !$btnContainer.data('has-buttons')) {
			try {
				addToSlide(sectionId, pageId);

				// Setup an event for animating in the buttons
				buttonTime = stage.getLabelPosition('Buttons');
				Edge.Symbol.bindTriggerAction(comp.current.id, 'stage', 'Default Timeline', buttonTime, buttonTriggerHandler);

				if (isFirstSlide) {
					stage.play();
				}
			} catch(e) {
				// not empty
				console.log('error: ',  e);
			}
		}
	}

	function buttonTriggerHandler() {

		var $buttons = $btnContainer.find('.button');
		console.log('show buttons', comp.current.id);

		$btnContainer.addClass('is-loaded');
		if ($btnContainer.hasClass('stagger')) {
			TweenMax.staggerFromTo($buttons, 1, {opacity: 0}, {opacity: 1}, 0.25);
		}
	}
}

/**
 * Cleans the button data to remove buttons to which the current user does not have access.
 */
function cleanData(sectionId) {

	var i;
	var pageId;
	var curItem;

	for (pageId in buttonData[sectionId]) {

		curItem = buttonData[sectionId][pageId];

		for (i = 0; i < curItem.length; i++) {
			if (appData.userType.trim()) {
				if (curItem[i].restrict && curItem[i].restrict.indexOf(appData.userType) !== -1) {
					curItem[i] = {};
				}
			}
		}
	}
}

/**
 * Adds buttons to the current slide.
 * @param {String} sectionId The sectionId of the section in which the current slide lives. (e.g. Medical, Dental, etc.)
 * @param {String} pageId The pageId of the current slide.
 */
function addToSlide(sectionId, pageId) {

	var tmpl;

	// Add the buttons
	if (buttonData[sectionId] && buttonData[sectionId][pageId]) {

		tmpl = buttonTmpl({buttons: buttonData[sectionId][pageId]});

		$btnContainer
			.append(tmpl)
			.data('has-buttons', true);
	}
}

/**
 * Update the $btnContainer object to be pointing at the current composition object's containing element.
 */
function updateContainer() {

	$btnContainer = $('.' + comp.current.id + ' .button-list');
	hideButtons();
}

function hideButtons() {

	if ($btnContainer.hasClass('stagger')) {
		$btnContainer.find('.button').css({opacity: 0});
	}
}

module.exports = buttons;
