/**
 * Menu module
 *
 * @module modules/menu
 * @description Initializes menu and handles all menu events.
 */

'use strict';

var menuView = require('views/menu');

var $menuWrapper = $('.side-menu');

var menu = {
	setup: setup,
	toggle: toggleClickHandler,
	isOpened: isOpened
};

////////////

/**
 * Initializes and sets up event handlers for the menu.
 */
function setup() {

	$menuWrapper.html(menuView);
	$.hook('menu-toggle').on('click', toggleClickHandler);
	$.hook('menu-item').on('click', subMenuItemClickHandler);
}

/**
 * Event handler for toggling the menu open/closed.
 * @param  {jQuery.Event} e jQuery event object.
 */
function toggleClickHandler(e) {

	e && e.preventDefault();
	$menuWrapper.toggleClass('is-opened');
}

/**
 * Returns true if the menu is currently open. Otherwise, returns false.
 * @return {Boolean} Boolean indicating whether the menu is opened.
 */
function isOpened() {

	return $menuWrapper.hasClass('is-opened');
}

/**
 * Event handler for toggling sub-menu items open/closed.
 * @param  {jQuery.Event} e jQuery event object
 */
function subMenuItemClickHandler(e) {

	var $curSubMenu = $(this).siblings('.sub-menu');

	e && e.preventDefault();

	// Close all other sub-menus
	$('.sub-menu').not($curSubMenu).slideUp();

	// Toggle current sub-menu
	$curSubMenu.slideToggle();
}

module.exports = menu;
