/**
 * jQuery Data Hook module
 *
 * @module lib/data-hook
 * @description Sets up the jQuery data hook plugin that is outlined {@link http://www.sitepoint.com/effective-event-binding-jquery/}
 * Example usage:<br>
 * <pre><code>
 * // Create button with a data-hook attribute
 * &lt;button data-hook=&quot;nav-menu-toggle&quot;&gt;Toggle Nav&lt;/button&gt;
 *
 * // Select that button as follows:
 * $.hook('nav-menu-toggle');
 *
 * // Register an event handler as follows:
 * $.hook('nav-menu-toggle').on('click', function() {});
 * </code></pre>

 */

'use strict';

var dataHook = {
	setup: setup
};

/**
 * Registers the $.hook jQuery plugin
 */
function setup() {

	$.extend({
		hook: function(hookName) {
			var selector;
			if (!hookName || hookName === '*') {
				// select all data-hooks
				selector = '[data-hook]';
			} else {
				// select specific data-hook
				selector = '[data-hook~="' + hookName + '"]';
			}
			return $(selector);
		}
	});
}

module.exports = dataHook;
