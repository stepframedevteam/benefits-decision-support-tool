/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'title',
                            type: 'image',
                            rect: ['111px', '76px', '233px', '29px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"title.png",'0px','0px']
                        },
                        {
                            id: 'sec01',
                            type: 'image',
                            rect: ['111px', '134px', '720px', '75px', 'auto', 'auto'],
                            opacity: '0.39837398373984',
                            fill: ["rgba(0,0,0,0)",im+"sec01.png",'0px','0px']
                        },
                        {
                            id: 'sec02',
                            type: 'image',
                            rect: ['111px', '220px', '720px', '75px', 'auto', 'auto'],
                            opacity: '0.39837398373984',
                            fill: ["rgba(0,0,0,0)",im+"sec02.png",'0px','0px']
                        },
                        {
                            id: 'sec03',
                            type: 'image',
                            rect: ['111px', '305px', '720px', '75px', 'auto', 'auto'],
                            opacity: '0.39837398373984',
                            fill: ["rgba(0,0,0,0)",im+"sec03.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 10395,
                    autoPlay: true,
                    data: [
                        [
                            "eid15",
                            "left",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec01}",
                            '111px',
                            '131px'
                        ],
                        [
                            "eid9",
                            "opacity",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec02}",
                            '0',
                            '0.39837398373984'
                        ],
                        [
                            "eid25",
                            "opacity",
                            5000,
                            648,
                            "easeOutCubic",
                            "${sec02}",
                            '0.3983739912509918',
                            '1'
                        ],
                        [
                            "eid13",
                            "left",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec03}",
                            '111px',
                            '131px'
                        ],
                        [
                            "eid17",
                            "left",
                            0,
                            750,
                            "easeOutCubic",
                            "${title}",
                            '111px',
                            '131px'
                        ],
                        [
                            "eid7",
                            "opacity",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec01}",
                            '0',
                            '0.39837398373984'
                        ],
                        [
                            "eid24",
                            "opacity",
                            2000,
                            648,
                            "easeOutCubic",
                            "${sec01}",
                            '0.3983739912509918',
                            '1'
                        ],
                        [
                            "eid11",
                            "opacity",
                            0,
                            750,
                            "easeOutCubic",
                            "${title}",
                            '0',
                            '1'
                        ],
                        [
                            "eid5",
                            "opacity",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec03}",
                            '0',
                            '0.39837398373984'
                        ],
                        [
                            "eid23",
                            "opacity",
                            7295,
                            648,
                            "easeOutCubic",
                            "${sec03}",
                            '0.3983739912509918',
                            '1'
                        ],
                        [
                            "eid19",
                            "left",
                            0,
                            750,
                            "easeOutCubic",
                            "${sec02}",
                            '111px',
                            '131px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("dental_intro_edgeActions.js");
})("dental_intro");
