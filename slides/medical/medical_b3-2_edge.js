/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'buildings',
                            type: 'image',
                            rect: ['394px', '0', '567px', '471px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"buildings.png",'0px','0px']
                        },
                        {
                            id: 'Rectangle',
                            type: 'rect',
                            rect: ['0px', '382px', '981px', '93px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(9,61,50,1.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'RectangleCopy',
                            type: 'rect',
                            rect: ['0px', '407px', '981px', '68px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(29,184,150,1.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Buttons',
                            type: 'rect',
                            rect: ['84px', '101px', '299px', '202px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "button-list"
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['559px', '150px', '311px', '130px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​Preventive care</p>",
                            font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(29,184,150,1.00)", "normal", "none", "", "break-word", "normal"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2588,
                    autoPlay: true,
                    labels: {
                        "Buttons": 1750
                    },
                    data: [
                        [
                            "eid12",
                            "opacity",
                            0,
                            368,
                            "linear",
                            "${Rectangle}",
                            '0',
                            '1'
                        ],
                        [
                            "eid138",
                            "color",
                            1392,
                            0,
                            "linear",
                            "${Text}",
                            'rgba(29,184,150,1.00)',
                            'rgba(29,184,150,1.00)'
                        ],
                        [
                            "eid134",
                            "top",
                            1392,
                            1196,
                            "linear",
                            "${Text}",
                            '101px',
                            '150px'
                        ],
                        [
                            "eid8",
                            "opacity",
                            0,
                            368,
                            "linear",
                            "${RectangleCopy}",
                            '0',
                            '1'
                        ],
                        [
                            "eid2",
                            "opacity",
                            250,
                            750,
                            "easeOutCubic",
                            "${buildings}",
                            '0',
                            '1'
                        ],
                        [
                            "eid131",
                            "top",
                            2483,
                            105,
                            "linear",
                            "${buildings}",
                            '0px',
                            '-5px'
                        ],
                        [
                            "eid126",
                            "background-color",
                            2588,
                            0,
                            "linear",
                            "${Buttons}",
                            'rgba(255,255,255,0.00)',
                            'rgba(255,255,255,0.00)'
                        ],
                        [
                            "eid14",
                            "left",
                            250,
                            750,
                            "easeOutCubic",
                            "${buildings}",
                            '394px',
                            '414px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("medical_b3-2_edgeActions.js");
})("medical_b3-2");
