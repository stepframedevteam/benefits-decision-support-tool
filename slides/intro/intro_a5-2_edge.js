/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Buttons',
                            type: 'rect',
                            rect: ['84px', '101px', '299px', '202px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "button-list"
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['521px', '161px', '354px', '172px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">I'm not sure if my base pay is competitive</p>",
                            font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(45,82,74,1.00)", "normal", "none", "", "break-word", "normal"],
                            textStyle: ["", "", "28px", "", ""]
                        },
                        {
                            id: 'Company_Name',
                            type: 'rect',
                            rect: ['834px', '8px', '132px', '93px', 'auto', 'auto'],
                            opacity: '0.03252032399177551',
                            fill: ["rgba(192,192,192,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "hook_company-name"
                        },
                        {
                            id: 'Text2',
                            type: 'text',
                            rect: ['668', '-54', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​</p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(29,184,150,1)", "400", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["", "", "", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            sizeRange: ['0px','981px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2588,
                    autoPlay: true,
                    labels: {
                        "Buttons": 1750
                    },
                    data: [
                        [
                            "eid126",
                            "background-color",
                            2588,
                            0,
                            "linear",
                            "${Buttons}",
                            'rgba(255,255,255,0.00)',
                            'rgba(255,255,255,0.00)'
                        ],
                        [
                            "eid160",
                            "height",
                            2074,
                            0,
                            "linear",
                            "${Text}",
                            '172px',
                            '172px'
                        ],
                        [
                            "eid140",
                            "opacity",
                            1000,
                            392,
                            "linear",
                            "${Company_Name}",
                            '0.032520',
                            '1'
                        ],
                        [
                            "eid149",
                            "opacity",
                            1392,
                            358,
                            "linear",
                            "${Text}",
                            '0.000000',
                            '0.99186991869919'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("intro_a5-2_edgeActions.js");
})("intro_a5-2");
