/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Buttons',
                            type: 'rect',
                            rect: ['141px', '360px', '596px', '101px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "button-list inline"
                        },
                        {
                            id: 'myVideo',
                            type: 'video',
                            tag: 'video',
                            rect: ['141px', '17px', '596px', '335px', 'auto', 'auto'],
                            controls: 'controls',
                            opacity: '0',
                            userClass: "myVideo",
                            source: [vid+"HCA-Benefits-are-not-very-good.mp4"],
                            preload: 'auto'
                        },
                        {
                            id: 'Text2',
                            type: 'text',
                            rect: ['668', '-54', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​</p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(29,184,150,1)", "400", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["", "", "", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            sizeRange: ['0px','981px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 750,
                    autoPlay: true,
                    labels: {
                        "Buttons": 250
                    },
                    data: [
                        [
                            "eid182",
                            "height",
                            750,
                            0,
                            "linear",
                            "${myVideo}",
                            '335px',
                            '335px'
                        ],
                        [
                            "eid176",
                            "left",
                            750,
                            0,
                            "linear",
                            "${myVideo}",
                            '141px',
                            '141px'
                        ],
                        [
                            "eid183",
                            "width",
                            750,
                            0,
                            "linear",
                            "${myVideo}",
                            '596px',
                            '596px'
                        ],
                        [
                            "eid172",
                            "opacity",
                            0,
                            514,
                            "linear",
                            "${myVideo}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid181",
                            "top",
                            750,
                            0,
                            "linear",
                            "${myVideo}",
                            '17px',
                            '17px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("intro_a5-1_edgeActions.js");
})("intro_a5-1");
