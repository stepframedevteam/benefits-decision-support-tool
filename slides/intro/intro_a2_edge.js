/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Rectangle',
                            type: 'rect',
                            rect: ['0px', '0px', '981px', '475px', 'auto', 'auto'],
                            fill: ["rgba(216,141,141,1.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['521px', '161px', '354px', '82px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px; text-align: center; line-height: 25px;\">Is your sound on?<span style=\"background-color: rgba(75, 181, 158, 0); font-size: 40px;\"></span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 30px;\"></span></p>",
                            font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(45,82,74,1.00)", "normal", "none", "", "break-word", "normal"],
                            textStyle: ["", "", "28px", "", ""]
                        },
                        {
                            id: 'Buttons',
                            type: 'rect',
                            rect: ['84px', '101px', '299px', '202px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "button-list full-width stagger"
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            sizeRange: ['0px','981px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 1000,
                    autoPlay: true,
                    labels: {
                        "Buttons": 608
                    },
                    data: [
                        [
                            "eid161",
                            "opacity",
                            250,
                            358,
                            "linear",
                            "${Buttons}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid149",
                            "opacity",
                            250,
                            358,
                            "linear",
                            "${Text}",
                            '0.000000',
                            '0.99186991869919'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("intro_a2_edgeActions.js");
})("intro_a2");
