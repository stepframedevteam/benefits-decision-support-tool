/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Button-List',
                            type: 'rect',
                            rect: ['516px', '194px', '299px', '44px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "button-list"
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['516px', '74px', '420px', '110px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px; line-height: 35px;\">​<span style=\"font-size: 26px;\">If you want to talk {{companyName}}, then you've come to the right place. Are you ready?</span></p>",
                            userClass: "inject exit_up",
                            font: ['Arial, Helvetica, sans-serif', [25, "px"], "rgba(45,82,74,1.00)", "normal", "none", "", "break-word", "normal"],
                            textStyle: ["", "", "32px", "", ""]
                        },
                        {
                            id: 'Text2',
                            type: 'text',
                            rect: ['668', '-54', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​</p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(29,184,150,1)", "400", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Text3',
                            type: 'text',
                            rect: ['516px', '266px', '384px', '93px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 15px;\">Plan information is updated periodically. You can view the terms of the plans in the Summary Plan Descriptions. If you have questions about the HCA benefit programs or your specific situation, call your benefit plan provider.</span></p>",
                            align: "left",
                            userClass: "exit_down",
                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(43,86,76,1.00)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "19px", "", "none"]
                        },
                        {
                            id: 'hca-logo',
                            type: 'image',
                            rect: ['130px', '160px', '293px', '95px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"hca-logo.png",'0px','0px'],
                            userClass: "exit_left"
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '981px', '475px', 'auto', 'auto'],
                            sizeRange: ['0px','981px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 500,
                    autoPlay: false,
                    data: [
                        [
                            "eid175",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${Button-List}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid156",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${Text3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid182",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${hca-logo}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid184",
                            "top",
                            0,
                            500,
                            "linear",
                            "${hca-logo}",
                            '160px',
                            '171px'
                        ],
                        [
                            "eid149",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${Text}",
                            '0.000000',
                            '0.99186991869919'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("intro_a1_edgeActions.js");
})("intro_a1");
